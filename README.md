**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**  
# Course:LIS4381- Mobile App Development
## Your Name: Nhi Tran

### Assignment # Requirements: A01

*Three Parts:*
1. Distributed version control with git and bitbucket
2. Development Installations
3. Chapter Questions (Chs. 1,2)

#### README.md file should include the following items:

* Screenshot of ampps installation running: My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial above
* [link to localhost with ampps running](localhost:80)

#### Git commands w/short descriptions:

1. git init  
Creates or reinitializes an existing Git repository to empty. It creates an empty .git directory and initializes it with subdirectories (objects, refs/heads, refs/ tags, and template flags). 
2. git status  
Shows the working tree status. Their may be different branches or paths to the master branch, and displays which branch user is currently working on.
3. git add  
It is the step before committing contents and known as the "staging area". It updates the index using the current content found in the working tree, to prepare the content staged for the next commit. The index holds any new or modified files before commiting.
4. git commit  
Records the changes to the repository and shows the current contents of the index along with a log message from the user. It does not include git add or git rm files since they are still in the "staging area" and has not been modified to the repository. 
5. git push  
Command pushes objects and updates the remotes references.
6. git pull  
Updates the local git repository by pulling changes from the remote repository to the local computer. It fetches from and integrates with another repository or a local branch.
7. one additional git command: git clone  
Creates a working copy of a local repository (git clone /path/to/repository) or bring a repository from a remote server to the local (git clone username@host:/path/to/repository)



#### Assignment Screenshots: #####
*Screenshot of AMPPS running PHP installation*
![Screen Shot 2016-05-16 at 10.22.53 PM.png](https://bitbucket.org/repo/zdG9qp/images/1588977727-Screen%20Shot%202016-05-16%20at%2010.22.53%20PM.png)
![Screen Shot 2016-05-16 at 10.23.30 PM.png](https://bitbucket.org/repo/zdG9qp/images/3466719984-Screen%20Shot%202016-05-16%20at%2010.23.30%20PM.png)
-------------------------
*Screenshot of java hello*
![Screen Shot 2016-01-17 at 4.48.25 PM.png](https://bitbucket.org/repo/zdG9qp/images/3145436284-Screen%20Shot%202016-01-17%20at%204.48.25%20PM.png)
*Screenshot of Android Studio- My first App*
*Tutorial Screenshots*
![Screen Shot 2016-05-11 at 12.33.44 PM.png](https://bitbucket.org/repo/zdG9qp/images/2841277270-Screen%20Shot%202016-05-11%20at%2012.33.44%20PM.png)
![Screen Shot 2016-05-11 at 12.33.53 PM.png](https://bitbucket.org/repo/zdG9qp/images/850550342-Screen%20Shot%202016-05-11%20at%2012.33.53%20PM.png)
[Screen Shot 2016-05-11 at 12.33.59 PM.png](https://bitbucket.org/repo/zdG9qp/images/1633398863-Screen%20Shot%202016-05-11%20at%2012.33.59%20PM.png)![Screen Shot 2016-05-11 at 12.34.06 PM.png](https://bitbucket.org/repo/zdG9qp/images/1818359496-Screen%20Shot%202016-05-11%20at%2012.34.06%20PM.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ndt14b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ndt14b/myteamquotes/